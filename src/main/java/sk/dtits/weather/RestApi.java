package sk.dtits.weather;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@RequestMapping("/api")
public class RestApi {
    @GetMapping("/locations/{location}")
    public Weather getWeather(@PathVariable String location) {
        var weather = new Weather();
        weather.setLocation(location);
        weather.setDegrees(new BigDecimal("12.5"));
        weather.setDescription("Windy and sunny");
        return weather;
    }
}

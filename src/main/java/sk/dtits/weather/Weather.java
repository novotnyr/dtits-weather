package sk.dtits.weather;

import java.math.BigDecimal;

public class Weather {
    private String location;

    private BigDecimal degrees;

    private String description;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public BigDecimal getDegrees() {
        return degrees;
    }

    public void setDegrees(BigDecimal degrees) {
        this.degrees = degrees;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
